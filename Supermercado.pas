{
* Trabajo Final: Supermercado (c)2014.
* UADER FCYT Lic. Sistemas
* Alan Asmis, Franco Cafferata, Leonardo Chaia.
* https://bitbucket.org/lospibes/supermercado
* 
* This work is licensed under the Creative Commons 
* Attribution-NonCommercial-NoDerivatives 4.0 International License. 
* To view a copy of this license, visit 
* http://creativecommons.org/licenses/by-nc-nd/4.0/.
* }
PROGRAM Supermercado;
USES CRT, SuperCliente, SuperConfig, SuperGraphics, SuperProductos, SuperVentas;

FUNCTION mostrarMenuPrincipal : CHAR;
BEGIN
	infoBox('Seleccione una opcion.. ', MENU_COLOR, FALSE);
	tituloBox('Menu Principal', MENU_COLOR);
	Box(1,79,4,19);
	Writeln('1 - ABM Clientes');
	Writeln('2 - ABM Productos');
	Writeln('3 - Ventas');
	Writeln;
	Writeln('0 - Salir');
	mostrarMenuPrincipal:= Readkey;
END;

{Programa Principal}
VAR 
	sel : CHAR;
BEGIN
	//introBox;
	REPEAT
		cursorOff;
		sel := mostrarMenuPrincipal;
		cursorOn;
		CASE sel OF 
			'1' : ABMClientes;
			'2'	: ABMProductos;
			'3' : ABMVentas;
		END;
	UNTIL sel IN ['0', KEY_ESC];
	infoBox('', MENU_COLOR, FALSE);
	tituloBox('CHAU CHAU ADIOS', MENU_COLOR);
	Box(1,79,4,19);
END.
