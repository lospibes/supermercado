{
* Trabajo Final: Supermercado (c)2014.
* Alan Asmis, Franco Cafferata, Leonardo Chaia.
* https://bitbucket.org/lospibes/supermercado
* 
* This work is licensed under the Creative Commons 
* Attribution-NonCommercial-NoDerivatives 4.0 International License. 
* To view a copy of this license, visit 
* http://creativecommons.org/licenses/by-nc-nd/4.0/.
* }
UNIT SuperConfig;
{*
* Archivo de configuración global
* }
INTERFACE
	USES CRT;
	CONST
		DATA_FOLDER = 'data';
		
		DEFAULT_COLOR = LightGray;
		CLIENTES_COLOR = LIGHTCYAN;
		PRODUCTOS_COLOR = LIGHTBLUE;
		VENTAS_COLOR = LIGHTMAGENTA;
		MENU_COLOR = YELLOW;
		
		DEFAULT_TOTAL_ARRAY = 1024;
		
	TYPE
		T_STR = STRING[32];
		T_LONG_STR = STRING[128];
		
IMPLEMENTATION

END.
