{
* Trabajo Final: Supermercado (c)2014.
* Alan Asmis, Franco Cafferata, Leonardo Chaia.
* https://bitbucket.org/lospibes/supermercado
* 
* This work is licensed under the Creative Commons 
* Attribution-NonCommercial-NoDerivatives 4.0 International License. 
* To view a copy of this license, visit 
* http://creativecommons.org/licenses/by-nc-nd/4.0/.
* }
UNIT SuperVentas;
INTERFACE
	USES SysUtils, TypInfo, CRT, dateUtils, SuperConfig, SuperGraphics, SuperCliente, SuperProductos;
	CONST
		TOT_VENTAS = DEFAULT_TOTAL_ARRAY;
		TOT_LIN_VENTAS = 100;
		VENTAS_ARCH = DATA_FOLDER + '/archVentas.dat';
		VENTAS_POR_PAG = 16;
		
	TYPE
		T_VENTA_ID = -1..TOT_VENTAS;
		T_LIN_VENTA_ID = -1..TOT_LIN_VENTAS;
		
		T_PROD_AUX = Record
			idLinea		:	T_LIN_VENTA_ID;
			idProducto	:	T_PROD_ID;
			cant		:	WORD;
			subtotal	:	REAL;
		END;
		
		T_PROD_AUX_ARR = ARRAY[T_LIN_VENTA_ID] OF T_PROD_AUX;
		T_FORMA_PAGO = (NULL, CREDITO, DEBITO, EFECTIVO);

		T_VENTA = RECORD
			id			: T_VENTA_ID;
			idCliente	: T_CLI_ID;
			fecha		: TDateTime;
			lineaVentas : T_PROD_AUX_ARR;
			limLineaVentas: T_LIN_VENTA_ID;
			total		: REAL;
			formaPago	: T_FORMA_PAGO;
			estaActivo  : BOOLEAN;
		END;

		T_VENTA_ARR = ARRAY[T_VENTA_ID] OF T_VENTA;
		T_VENTA_ARCH = FILE OF T_VENTA;

	{ Procedimientos publicos}
	PROCEDURE ABMVentas;
IMPLEMENTATION

	VAR
		archVenta : T_VENTA_ARCH;
		ultIDVenta : T_VENTA_ID;
		listaVentas : T_VENTA_ARR;
	
	PROCEDURE inicializar;
	BEGIN
		assign(archVenta, VENTAS_ARCH);
		IF NOT DirectoryExists(DATA_FOLDER) THEN
			mkdir(DATA_FOLDER);

		IF NOT FileExists(VENTAS_ARCH) THEN
			reWrite(archVenta)
		ELSE
			reSet(archVenta);

		ultIDVenta:= -1;
		WHILE NOT EOF(archVenta) DO
		BEGIN
			inc(ultIDVenta);
			seek(archVenta, ultIDVenta);
			read(archVenta, listaVentas[ultIDVenta]);
		END;
		close(archVenta);
	END;
	
	FUNCTION ventaVacia : T_VENTA;
	VAR
		v 	:	T_VENTA;
	BEGIN
		WITH v DO BEGIN
			id 			:= -1;
			idCliente	:=	-1;
			//fecha		:= EncodeDateTime(0,0,0,0,0,0,0);
			total		:= 0;
			formaPago	:= NULL;
			estaActivo  := FALSE;
		END;
		ventaVacia:= v;
	END;
	
	FUNCTION fPagoToStr(fPago : T_FORMA_PAGO) : STRING;
	BEGIN
		fPagoToStr:= GetEnumName(TypeInfo(T_FORMA_PAGO),ord(fPago));
	END;

	FUNCTION leerFormaPago : T_FORMA_PAGO;
	VAR
		fp: T_FORMA_PAGO;
		key	: CHAR;
	BEGIN
		tituloBox('Seleccionar Forma de Pago', VENTAS_COLOR);
		Box(1,79,4,19);
		Writeln('Seleccione una forma de pago');
		Writeln;
		Writeln(' 1 - EFECTIVO');
		Writeln(' 2 - TARJETA CREDITO');
		Writeln(' 3 - TARJETA DEBITO');
		Writeln;
		infoBox('Opcion', VENTAS_COLOR, FALSE);
		key:= Readkey;
		CASE key OF
			'1'	: fp:= EFECTIVO;
			'2'	: fp:= CREDITO;
			'3' : fp:= DEBITO;
			ELSE
				fp:=NULL;
		END;
		leerFormaPago:= fp;
	END;
	
	PROCEDURE mostrarVenta(Venta : T_VENTA; unaLinea : BOOLEAN);
	VAR
		cli : T_CLIENTE;
		prod : T_PRODUCTO;
		i	:	T_LIN_VENTA_ID;
	BEGIN
		cli:= getCliente(venta.idCliente);
		IF unaLinea THEN BEGIN
			WITH venta DO BEGIN
				IF NOT estaActivo THEN
					TextColor(RED);
				Write('| ',id,'  ');
				GoToXY(8,WhereY);
				Write('| ',cli.apellido, ' ', cli.nombre);
				GoToXY(35,WhereY);
				Write('| ', dateTimeToStr(fecha));
				GoToXY(48,WhereY);
				Write('| ', fPagoToStr(formaPago));
				GoToXY(58, WhereY);
				Write('|');
				TextColor(Green);
				Write(' $',total:6:2,' .-');
				GoToXY(73,WhereY);
				TextColor(DEFAULT_COLOR);
				Writeln('|');
			END;
		END 
		ELSE BEGIN 
			WITH Venta DO BEGIN
				tituloBox('Venta ID #' + intToStr(id) + ' cliente ' + cli.nombre
						+ ' ' + cli.apellido + ' ' + dateTimeToStr(venta.fecha), VENTAS_COLOR);
				Box(1,79,4,19,1,1);
				TextColor(White);
				Write('| ID ');
				GoToXY(7, WhereY);
				Write('| ID Prod');
				GoToXY(18,WhereY);
				Write('|          DESCRIPCION');
				GoToXY(57,WhereY);
				Write('|  CANT');
				GoToXY(65,WhereY);
				Write('|  SUBTOTAL');
				//GoToXY(88,WhereY);
				Writeln('  |');
				TextColor(DEFAULT_COLOR);
				FOR i:= 0 TO limLineaVentas DO 
				BEGIN
					WITH Venta.lineaVentas[i] DO BEGIN
						prod:= getProducto(idProducto);
						Write('| #', idLinea);
						GoToXY(7, WhereY);
						Write('|   #', prod.id);
						GoToXY(18,WhereY);
						Write('| ', prod.desc);
						GoToXY(57,WhereY);
						Write('| ');
						Write(cant);
						TextColor(DEFAULT_COLOR);
						GoToXY(65, WhereY);
						Write('| ');
						TextColor(Green);
						Write('$',subtotal:8:2);
						TextColor(DEFAULT_COLOR);
						Write('.-');
						//GoToXY(88,WhereY);
						Writeln('|');
					END;
				END;
				infoBox('F PAGO: '+ fPagoToStr(formaPago) + '                                       TOTAL  $' + formatFloat('#######.##',total)+ '.-', VENTAS_COLOR, FALSE);
			END;
		END;
	END;
	
	PROCEDURE mostrarVenta(Venta : T_VENTA);
	BEGIN
		mostrarVenta(Venta, FALSE);
	END;
	
	PROCEDURE mostrarListaVentas(VAR lista : T_VENTA_ARR; limite : T_VENTA_ID; mostrarInactivos : BOOLEAN; titulo : T_STR);
		PROCEDURE escribirTablaSup;
		BEGIN
			TextColor(White);
			Write('| ID  ');
			GoToXY(8,WhereY);
			Write('|       CLIENTE ');
			GoToXY(35,WhereY);
			Write('| FECHA ');
			GoToXY(48,WhereY);
			Write('| F.PAGO ');
			GoToXY(58, WhereY);
			Write('|   TOTAL');
			GoToXY(73,WhereY);
			Writeln('|');
			TextColor(DEFAULT_COLOR);
		END;
	VAR
		i: T_VENTA_ID;
		cantMostrada: 0..VENTAS_POR_PAG;
		key : CHAR;
	BEGIN
		cursorOff;
		tituloBox(titulo + ' (' + #17 + ' ' + #16 + ' para navegar, ESC salir)', VENTAS_COLOR);
		Box(1,79,4,19);
		escribirTablaSup;
		cantMostrada:= 0; 
		i:= -1;
		key:= KEY_ENTER;
		IF limite < 0 THEN
		BEGIN
			Writeln('No hay ventas ingresados..');
			infoBox('ERROR: No hay productos ingresados', RED, TRUE);
			Readln;
		END
		ELSE
			REPEAT
				IF i < limite THEN
					inc(i);
				WITH lista[i] DO
					IF estaActivo OR (NOT estaActivo AND mostrarInactivos) THEN
					BEGIN
						inc(cantMostrada);
						mostrarVenta(lista[i], TRUE);
						IF (cantMostrada = VENTAS_POR_PAG) OR ((cantMostrada <> VENTAS_POR_PAG) AND (i = limite)) THEN
						BEGIN
							infoBox('Mostrando '+ intToStr(cantMostrada)+ ' de '+intToStr(limite + 1)+' a partir del ID#'+ intToStr(i - cantMostrada), VENTAS_COLOR, FALSE);
							key := Readkey;
							IF key <> KEY_ESC THEN
							BEGIN
								key := Readkey;
								IF key = KEY_LEFT THEN
									IF ((i - (cantMostrada + VENTAS_POR_PAG)) > 0 ) THEN
										i:= i - (cantMostrada + VENTAS_POR_PAG)
									ELSE
										i:=-1
								ELSE IF (i = limite) THEN BEGIN
									IF (i - cantMostrada > 0) THEN
										i:= i - cantMostrada;
									sound(1);
								END;
								Box(1,79,4,19);
								escribirTablaSup;
								cantMostrada:= 0;
							END;
						END;
					END;
			UNTIL (key = KEY_ESC);
		cursorOn;
	END;
	
	PROCEDURE mostrarListaVentas(VAR lista : T_VENTA_ARR; limite : T_VENTA_ID; mostrarInactivos : BOOLEAN);
	BEGIN
		mostrarListaVentas(lista, limite, FALSE, 'Lista Ventas');
	END;
	
	PROCEDURE mostrarListaVentas;
	BEGIN
		mostrarListaVentas(listaVentas, ultIDVenta, FALSE);
	END;
	
	PROCEDURE listaVentasFecha (VAR arr: T_VENTA_ARR;  limite : T_VENTA_ID);
	VAR
		i,j : T_VENTA_ID;
		auxVenta : T_VENTA;
		cambUltVueltA: BOOLEAN;
	BEGIN
		i:= -1; cambUltVuelta:= TRUE;
		WHILE (i < ultIDVenta -1) AND (i < TOT_VENTAS) AND cambUltVuelta DO
		BEGIN
			inc(i);
			cambUltVuelta:= FALSE;
			FOR j:= 0 TO limite - i -1 DO
				IF compareDate(arr[j].fecha, arr[j + 1].fecha) > 0 THEN
				BEGIN
					cambUltVuelta:= TRUE;
					auxVenta := arr[j];
					arr[j] := arr[j + 1];
					arr[j + 1] := auxVenta;
				END;
		END;
	END;
	PROCEDURE listaVentasFecha (VAR arr: T_VENTA_ARR);
	BEGIN
		listaVentasFecha(arr, ultIDVenta);
	END;
	
	PROCEDURE listaVentasCliente (VAR arr: T_VENTA_ARR);
	VAR
		i,j : T_VENTA_ID;
		auxVenta : T_VENTA;
		cambUltVueltA: BOOLEAN;
	BEGIN
		i:= -1; cambUltVuelta:= TRUE;
		WHILE (i < ultIDVenta -1) AND (i < TOT_VENTAS) AND cambUltVuelta DO
		BEGIN
			inc(i);
			cambUltVuelta:= FALSE;
			FOR j:= 0 TO ultIDVenta - i -1 DO BEGIN
				IF arr[j].idCliente > arr[j + 1].idCliente THEN
				BEGIN
					cambUltVuelta:= TRUE;
					auxVenta := arr[j];
					arr[j] := arr[j + 1];
					arr[j + 1] := auxVenta;
				END;
			END;
		END;
	END;
	
	PROCEDURE buscarVentasFecha(VAR arrIn : T_VENTA_ARR; limIn : T_VENTA_ID; FechaA, FechaB : TDateTime; VAR arrOut : T_VENTA_ARR; VAR limOut : T_VENTA_ID);
	VAR
		arr : T_VENTA_ARR;
		i	: T_VENTA_ID;
	BEGIN
		arr:= arrIn;
		listaVentasFecha(arr, limIn);
		limOut:= -1;
		i:= -1;	
		WHILE (compareDate(arr[i].fecha, fechaB) <= 0) AND (i <= limIn) DO BEGIN
			inc(i);
			IF (compareDate(arr[i].fecha, fechaA) >= 0) THEN BEGIN
				inc(limOut);
				arrOut[limOut]:= arr[i];
			END;
			
		END;
	END;
	
	PROCEDURE buscarVentasFecha(FechaA, FechaB : TDateTime; VAR arrOut : T_VENTA_ARR; VAR limOut : T_VENTA_ID);
	BEGIN
		buscarVentasFecha(listaVentas, ultIDVenta, FechaA, FechaB, arrOut, limOut);
	END;
	
	FUNCTION mostrarBuscarVenta : T_VENTA;
	VAR
		auxID : INTEGER;
	BEGIN
		tituloBox('Buscar venta por ID', VENTAS_COLOR);
		Box(1,79,4,19);
		REPEAT
			Writeln('Buscar Venta (-1 para salir)');
			Write('Ingrese un ID Venta: ');
			Readln(auxID);
		UNTIL (auxID >= -1) AND (auxID <= ultIDVenta);
		IF auxID >= 0 THEN
			mostrarBuscarVenta:= listaVentas[auxID]	
		ELSE
			mostrarBuscarVenta:= ventaVacia;
	END;
	
	PROCEDURE buscarVentasCliente(buscado : T_CLIENTE; incInactivos : BOOLEAN; VAR arrOut : T_VENTA_ARR; VAR limOut : T_VENTA_ID);
	VAR
		arr : T_VENTA_ARR;
		primero, ultimo, medio, limInicio, limFin : T_VENTA_ID;
		auxCli	:	T_CLIENTE;
	BEGIN
		arr:= listaVentas;
		listaVentasCliente(arr);
		primero:=0;
		ultimo:=ultIDVenta;
		limOut:= -1;
		WHILE (primero <= ultimo) AND (limOut = -1) DO
		BEGIN
			medio:= (primero + ultimo) DIV 2;
			auxCli:= getCliente(arr[medio].idCliente);
			IF (auxCli.id = buscado.id) THEN BEGIN
				limInicio:= medio -1;
				limFin:= medio;
				WHILE (limInicio > 0) AND (arr[limInicio].idCliente = arr[medio].idCliente) DO BEGIN
					inc(limOut);
					arrOut[limOut]:= arr[limInicio];
					dec(limInicio);
				END;
				WHILE (limFin <= ultIDVenta) AND (arr[limFin].idCliente = arr[medio].idCliente) DO BEGIN
					inc(limOut);
					arrOut[limOut]:= arr[limFin];
					inc(limFin);
				END;
			END
			ELSE IF auxCli.id > buscado.id THEN
					ultimo:= medio - 1
				ELSE
					primero:= medio + 1;
		END;
	END;
	
	PROCEDURE altaBajaVenta(VAR venta : T_VENTA; NuevoEstado : BOOLEAN);
	BEGIN
		venta.estaActivo:= NuevoEstado;
		listaVentas[ultIDVenta]:= venta;
		Reset(archVenta);
		Seek(archVenta, venta.id);
		Write(archVenta, venta);
		Close(archVenta);
	END;
	
	PROCEDURE cargarVenta;
	VAR
		auxCli 	: T_CLIENTE;
		auxProd : T_PRODUCTO;
		auxVenta : T_VENTA;
		i		: T_LIN_VENTA_ID;
		auxProdAux	: T_PROD_AUX;
		auxCant		: WORD;
		salir : BOOLEAN;
	BEGIN
		i:= -1;
		salir:= FALSE;;
		auxCli := mostrarBuscarCliente;
		auxProd.id := -1;
		tituloBox('Nueva venta', VENTAS_COLOR);
		Box(1,79,4,19);
		IF auxCli.id >= 0 THEN
		BEGIN
			mostrarCliente(auxCli);
			Readln;
			REPEAT
				IF auxProd.id >= 0 THEN BEGIN
					tituloBox('Nueva venta', VENTAS_COLOR);
					Box(1,79,4,19);
					mostrarProducto(auxProd, FALSE);
					Writeln;
					Writeln('Cantidad: ');
					readln(auxCant);
					IF auxProd.stock < auxCant THEN	BEGIN
						Writeln('No hay stock suficiente para esta venta..');
						Readln;
					END
					ELSE IF auxCant > 0 THEN BEGIN
						inc(i);
						auxProdAux.idLinea:= i;
						auxProdAux.cant:= auxCant;
						auxProdAux.subtotal:= auxProd.precio * auxProdAux.cant;
						auxProdAux.idProducto := auxProd.id;
						auxVenta.lineaVentas[i] := auxProdAux;
						auxVenta.total:= auxProdAux.subtotal + auxVenta.total;
						//modificarStock(auxProd, auxProdAux.cant); Movido fuera del ciclo
					END;
				END;
				auxProd:= mostrarBuscarProducto;
				IF auxProd.id < 0 THEN BEGIN
					Writeln('Salir? S/N    ENTER/ESC');
					salir:= Readkey IN ['S', 's', KEY_ENTER];
				END;
			UNTIL (i >= TOT_LIN_VENTAS) OR salir;
			IF i >= 0 THEN
			BEGIN
				auxVenta.idCliente := auxCli.id;
				auxVenta.fecha:= Today;
				REPEAT
					auxVenta.formaPago:= leerFormaPago;
				UNTIL auxVenta.formaPago IN [EFECTIVO, DEBITO, CREDITO];
				inc(ultIDVenta);
				auxVenta.id:= ultIDVenta;
				auxVenta.limLineaVentas:= i;
				altaBajaVenta(auxVenta, TRUE);
				//Ciclo aparte para evitar venta no finalizada y stock decrementado.
				FOR i:= 0 TO auxVenta.limLineaVentas DO BEGIN
					auxProd:= getProducto(auxVenta.lineaVentas[i].idProducto);
					modificarStock(auxProd, -auxVenta.lineaVentas[i].cant); //Negativo causa resta.
				END;
				mostrarVenta(auxVenta);
				Readln;
			END;
		END;
	END;
	
	PROCEDURE mostrarTotalesVendidos;
	VAR
		auxFechaA, auxFechaAB 	: TDateTime;
		i,limAux: T_VENTA_ID;
		total 	: REAL;
		auxArr	: T_VENTA_ARR;
	BEGIN
		total:= 0;
		tituloBox('Totales vendidos entre Rangos fechas', VENTAS_COLOR);
		Box(1,79,4,19);
		Write('Fecha Rango ');
		TextColor(White);
		Writeln('INICIO');
		TextColor(DEFAULT_COLOR);
		auxFechaA:= leerFecha;
		tituloBox('Totales vendidos desde el ' + DateTimeToStr(auxFechaA), VENTAS_COLOR);
		Box(1,79,4,19);
		Write('Fecha Rango ');
		TextColor(White);
		Writeln('FIN');
		TextColor(DEFAULT_COLOR);
		auxFechaAB:= leerFecha;
		tituloBox('Totales vendidos desde el ' + DateTimeToStr(auxFechaA) +  ' hasta el '+ DateTimeToStr(auxFechaAB), VENTAS_COLOR);
		Box(1,79,4,19);
		buscarVentasFecha(auxFechaA, auxFechaAB, auxArr, limAux);
		IF limAux >=0 THEN BEGIN
			FOR i:= 0 TO limAux DO	
				total:= auxArr[limAux].total + total;
			Write('Totales Vendidos ');
			escribirPrecio(total);
			Write(' desde el ',DateTimeToStr(auxFechaA), ' hasta el ', DateTimeToStr(auxFechaAB));
			//TODO: Ofrecer ver vistas consideradas para ese resultado?
			//mostrarListaVentas(auxArr, limAux, TRUE);
			infoBox('Totales Vendidos $'+ formatFloat('#######.##',total), VENTAS_COLOR, FALSE);
		END
		ELSE BEGIN
			Writeln('No se encontraron ventas desde el ',DateTimeToStr(auxFechaA), ' hasta el ', DateTimeToStr(auxFechaAB));
			infoBox('No se encontraron ventas dentro de esas fechas', VENTAS_COLOR, FALSE);
		END;
		Readkey;
	END;
	
	PROCEDURE mostrarVentasEnRangoFechas(CONST porCliente : BOOLEAN);
	VAR
		auxFechaA, auxFechaB 	: TDateTime;
		limAux, limIn  : T_VENTA_ID;
		arrAux, arrIn	: T_VENTA_ARR;
		auxCli 	: T_CLIENTE;
	BEGIN
		IF porCliente THEN BEGIN
			auxCli := mostrarBuscarCliente;
			buscarVentasCliente(auxCli, TRUE, arrIn, limIn);
		END
		ELSE BEGIN
			arrIn:= listaVentas;
			limIn:= ultIDVenta;
		END;
		tituloBox('Ver ventas entre Rangos fechas', VENTAS_COLOR);
		Box(1,79,4,19);
		Write('Fecha Rango ');
		TextColor(White);
		Writeln('INICIO');
		TextColor(DEFAULT_COLOR);
		auxFechaA:= leerFecha;
		tituloBox('Ver ventas desde el ' + DateTimeToStr(auxFechaA), VENTAS_COLOR);
		Box(1,79,4,19);
		Write('Fecha Rango ');
		TextColor(White);
		Writeln('FIN');
		TextColor(DEFAULT_COLOR);
		auxFechaB:= leerFecha;
		buscarVentasFecha(arrIn, limIn, auxFechaA, auxFechaB, arrAux, limAux);
		IF limAux >=0 THEN BEGIN
			mostrarListaVentas(arrAux, limAux, TRUE, DateTimeToStr(auxFechaA) + ' - ' + DateTimeToStr(auxFechaB));
		END
		ELSE BEGIN
			Writeln('No se encontraron ventas desde el ',DateTimeToStr(auxFechaA), ' hasta el ', DateTimeToStr(auxFechaB));
			infoBox('No se encontraron ventas dentro de esas fechas', VENTAS_COLOR, FALSE);
		END;
		Readkey;
	END;
	
	FUNCTION modificarProdAux(CONST prodAux : T_PROD_AUX) : T_PROD_AUX;
	VAR
		prod, newProd : T_PRODUCTO;
		key	 	: CHAR;
		auxCant : WORD;
		prodAuxOut : T_PROD_AUX;
	BEGIN
		prod:= getProducto(prodAux.idProducto);
		prodAuxOut:= prodAux;
		tituloBox('Modificar Linea Ventas Producto ID ' + IntToStr(prodAux.idProducto), VENTAS_COLOR);
		Box(1,79,4,19);
		mostrarProducto(prod, FALSE);
		Writeln('Cantidad actual: ', prodAux.cant);
		Writeln;
		Writeln('1 - Modificar Producto y Cantidad');
		Writeln('2 - Modificar Cantidad');
		Writeln('3 - Volver');
		Writeln;
		Writeln('Seleccione una opcion ');
		key := ReadKey;
		CASE key OF 
			'1' :	BEGIN
						newProd:= mostrarBuscarProducto;
						Box(1,79,4,19);
						mostrarProducto(newProd, FALSE);
						Writeln;
						Writeln('Cantidad: ');
						readln(auxCant);
						IF newProd.stock < auxCant THEN	BEGIN
							Writeln('No hay stock suficiente');
							Readln;
						END
						ELSE IF auxCant > 0 THEN BEGIN
							prodAuxOut.cant:= auxCant;
							prodAuxOut.subtotal:= newProd.precio * prodAuxOut.cant;
							prodAuxOut.idProducto := newProd.id;
							Writeln('Producto y Cantidad modificados con exito');
						END;
					END;
			'2' :	BEGIN
						Box(1,79,4,19);
						Writeln('Cantidad: ');
						readln(auxCant);
						IF (prod.stock + prodAux.cant) < auxCant THEN BEGIN //Tener en cuenta la cantidad total al validar
							Writeln('No hay stock suficiente');
							Readln;
						END
						ELSE IF auxCant > 0 THEN BEGIN
							prodAuxOut:= prodAux;
							prodAuxOut.cant:= auxCant;
							prodAuxOut.subtotal:= prod.precio * prodAuxOut.cant;
							Writeln;
							Writeln('Cantidad modificada con exito');
							Readln;
						END;
					END;
		END;
		modificarProdAux:= prodAuxOut;
	END;
	
	PROCEDURE modificarLineaDeVentas(VAR venta : T_VENTA);
	VAR
		auxIDLinVenta : T_LIN_VENTA_ID;
		auxProdAux	  : T_PROD_AUX;
		key			  : CHAR;
	BEGIN
		infoBox('Seleccione que desea modificar', VENTAS_COLOR, FALSE);
		tituloBox('Modificar Linea de Ventas, Venta ID ' + IntToStr(venta.id), VENTAS_COLOR);
		Box(1,79,4,19);
		Writeln('A continuacion se mostrara la linea de ventas.');
		Write('Ud puede salir en cualqueir momento utilizando');
		TextColor(White);
		Writeln('escape.');
		TextColor(DEFAULT_COLOR);
		Writeln('Luego de salir de la lista, se le solicitará un ID a modificar');
		Writeln('Donde podra modificar el producto y la cantidad');
		Readkey;
		REPEAT
			mostrarVenta(venta);
			Readkey;
			infoBox('', VENTAS_COLOR, FALSE);
			tituloBox('Seleccionar un ID a modificar' + IntToStr(venta.id), VENTAS_COLOR);
			Box(1,79,4,19);
			Write('Ingrese un ID: ');
			Readln(auxIDLinVenta);
			
			auxProdAux:= modificarProdAux(venta.lineaVentas[auxIDLinVenta]);
			venta.total:= venta.total - venta.lineaVentas[auxIDLinVenta].subtotal;
			venta.total:= auxProdAux.subtotal + venta.total;
			venta.lineaVentas[auxIDLinVenta]:= auxProdAux;
			tituloBox('Linea de ventas modificada!', VENTAS_COLOR);
			Box(1,79,4,19);
			TextColor(Green);
			Writeln('La linea de ventas ha sido modificada con exito');
			TextColor(DEFAULT_COLOR);
			Writeln;
			Writeln('Desea seguir modificando la lista de ventas? S/N   ESC/ENTER');
			key:= Readkey;
		UNTIL key IN['n',KEY_ESC];
		Box(1,79,4,19);
	END;
	
	PROCEDURE guardarVenta(VAR ventaOriginal, ventaNueva: T_VENTA );
	VAR
		i	:	T_LIN_VENTA_ID;
		linAuxOrig, linAuxNueva : T_PROD_AUX;
		huboCambios		: BOOLEAN;
	BEGIN
		infoBox('Seguro que desea aplicar los cambios?', Green, FALSE);
		tituloBox('Ingrese S/N ESC/ENTER para guardar ', VENTAS_COLOR);
		Box(1,79,4,19);
		Writeln('Guarda la venta modificada S/N ESC/ENTER?');
		IF Readkey IN ['s', KEY_ENTER] THEN BEGIN
			FOR i:= 0 TO ventaOriginal.limLineaVentas DO BEGIN
				huboCambios:= FALSE;
				linAuxOrig:= ventaOriginal.lineaVentas[i];
				linAuxNueva:= ventaNueva.lineaVentas[i];
				IF linAuxOrig.idProducto >= 0 THEN BEGIN
					IF linAuxNueva.idProducto >= 0 THEN BEGIN
						IF linAuxOrig.idProducto <> linAuxNueva.idProducto THEN BEGIN
							modificarStock(linAuxOrig.idProducto, linAuxOrig.cant); //Incrementar el stock original
							modificarStock(linAuxNueva.idProducto, -linAuxNueva.cant);// Decrementar el nuevo stock
							huboCambios:=TRUE
						END
						ELSE IF linAuxNueva.cant <> linAuxOrig.cant THEN BEGIN
							modificarStock(linAuxNueva.idProducto, linAuxOrig.cant - linAuxNueva.cant);
							huboCambios:=TRUE;
						END;
					END
					ELSE BEGIN
						linAuxNueva.idProducto:= -1;
						linAuxNueva.cant:= 0;
					END;
				END;
				IF huboCambios THEN
					ventaNueva.lineaVentas[i]:= linAuxNueva;
			END;
			Reset(archVenta);
			Seek(archVenta, ventaOriginal.id);
			Write(archVenta, ventaNueva);
			Close(archVenta);
			listaVentas[ventaOriginal.id]:= ventaNueva;
			infoBox('La venta ha sido modificada', Green, FALSE);
			tituloBox('Venta ID ' + IntToStr(ventaOriginal.id) + ' ha sido modificada.', Green);
			Box(1,79,4,19);
			TextColor(Green);
			Writeln('Venta ID ' + IntToStr(ventaOriginal.id) + ' ha sido modificada.');
		END
		ELSE BEGIN
			infoBox('No se realizaron cambios', RED, FALSE);
			tituloBox('Nada ha cambiado', VENTAS_COLOR);
			Box(1,79,4,19);
			TextColor(Red);
			Writeln('Nada ha cambiado. No se realizaron cambios');
		END;
		TextColor(DEFAULT_COLOR);
		Readkey;
	END;
	
	PROCEDURE mostrarModificarVenta(VAR ventaOriginal : T_VENTA);
	VAR
		key : CHAR;
		auxCli	 : T_CLIENTE;
		auxVenta : T_VENTA;
	BEGIN
		auxVenta:= ventaOriginal;
		auxCli := getCliente(auxVenta.idCliente);
		infoBox('Seleccione que desea modificar', VENTAS_COLOR, FALSE);
		tituloBox('Modificar Venta ID ' + IntToStr(auxVenta.id), VENTAS_COLOR);
		Box(1,79,4,19);
		REPEAT
			TextColor(White);
			Writeln('Modificar Venta');
			TextColor(DEFAULT_COLOR);
			Writeln;
			Writeln('1 - Cliente (', getFullName(auxCli),')');
			Writeln('2 - Fecha (',DateTimeToStr(auxVenta.fecha), ')');
			Writeln('3 - Forma de Pago (', fPagoToStr(auxVenta.formaPago) ,')');
			Writeln('4 - Linea de ventas (', auxVenta.limLineaVentas + 1,' productos)');
			TextColor(White);
			Writeln('5 - Guardar');
			TextColor(DEFAULT_COLOR);
			Writeln;
			Writeln('Seleccione una opcion');
			TextColor(White);
			Writeln('Estos cambios no seran guardados hasta que seleccione guardar.');
			TextColor(DEFAULT_COLOR);
			key:= readkey;
			CASE key OF
				'1' : 	BEGIN
							auxCli:= mostrarBuscarCliente;
							IF auxCli.id >= 0 THEN BEGIN
								auxVenta.idCliente:= auxCli.id;
								tituloBox('Venta ID ' + IntToStr(auxVenta.id) + ' modificada', VENTAS_COLOR);
								infoBox('El cliente ' + getFullName(getCliente(ventaOriginal.idCliente)) + ' ha sido cambiado por ' + getFullName(auxCli), VENTAS_COLOR, FALSE);
								Box(1,79,4,19);
								TextColor(Green);
								Writeln('El cliente ', getFullName(getCliente(ventaOriginal.idCliente)),' ha sido cambiado por ',getFullName(auxCli));
							END;
						END;
				'2' :	BEGIN
							tituloBox('Modificar Fecha de Venta ID ' + IntToStr(auxVenta.id), VENTAS_COLOR);
							Box(1,79,4,19);
							Writeln('Nueva fecha');
							auxVenta.fecha:=leerFecha;
							tituloBox('Venta ID ' + IntToStr(auxVenta.id) + ' modificada', VENTAS_COLOR);
							infoBox('La fecha ' + DateTimeToStr(ventaOriginal.fecha) + ' ha sido modificada por ' + DateTimeToStr(auxVenta.fecha), VENTAS_COLOR, FALSE);
							Box(1,79,4,19);
							TextColor(Green);
							Writeln('La fecha ', DateTimeToStr(ventaOriginal.fecha), ' ha sido modificada por ',DateTimeToStr(auxVenta.fecha));
						END;
				'3' :	BEGIN
							auxVenta.formaPago:= leerFormaPago;
							tituloBox('Venta ID ' + IntToStr(auxVenta.id) + ' modificada', VENTAS_COLOR);
							infoBox('La forma de pago  ' + fPagoToStr(ventaOriginal.formaPago) + ' ha sido modificada por ' + fPagoToStr(auxVenta.formaPago), VENTAS_COLOR, FALSE);
							Box(1,79,4,19);
							TextColor(Green);
							Writeln('La forma de pago  ', fPagoToStr(ventaOriginal.formaPago), ' ha sido modificada por ',fPagoToStr(auxVenta.formaPago));
						END;
				'4'	:	BEGIN
							modificarLineaDeVentas(auxVenta);
						END;
				'5'	:	BEGIN
							guardarVenta(ventaOriginal, auxVenta);
						END;
			END;
			TextColor(DEFAULT_COLOR);
			Writeln;
		UNTIL key IN [KEY_ESC, '5'];
		mostrarVenta(auxVenta);
		Readln;
	END;
	
	PROCEDURE mostrarBajaVenta;
	VAR
		auxVenta : T_VENTA;
		showMsg : BOOLEAN;		
		i		: T_LIN_VENTA_ID;
	BEGIN
		showMsg:= FALSE;
		infoBox('', VENTAS_COLOR, FALSE);
		tituloBox('Dar de baja una venta', VENTAS_COLOR);
		Box(1,79,4,19);
		auxVenta := mostrarBuscarVenta;
		IF auxVenta.id >= 0 THEN BEGIN
			TextColor(Red);
			Writeln('Esto eliminara la venta del sistema, ');
			TextColor(White);
			Writeln('El stock de cada producto sera restaurado.');
			TextColor(DEFAULT_COLOR);
			Write('Desea continuar? S/N ESC/ENTER');
			IF Readkey IN ['s', KEY_ENTER] THEN BEGIN			
				FOR i:=0 TO auxVenta.limLineaVentas DO
					modificarStock(auxVenta.lineaVentas[i].idProducto, auxVenta.lineaVentas[i].cant);
				altaBajaVenta(auxVenta, FALSE);
				tituloBox('La venta ha sido dada de baja', RED);
				Box(1,79,4,19);
				TextColor(White);
				Write('La venta ha sido ');
				TextColor(RED);
				Writeln('dada de baja');
				TextColor(Green);
				Writeln('Los productos han sido restaurados.');
				TextColor(DEFAULT_COLOR);
			END
			ELSE 
				showMsg:= TRUE;
		END
		ELSE
			showMsg:= TRUE;
			
		IF showMsg THEN BEGIN
			tituloBox('Nada ha cambiado!', VENTAS_COLOR);
			Box(1,79,4,19);
			TextColor(White);
			Write('La venta NO ha sido cambiada.');
			TextColor(DEFAULT_COLOR);
		END;
		Readkey;
	END;

	PROCEDURE ABMVentas;
	VAR
		key : CHAR;
		auxArr : T_VENTA_ARR;
		auxCli : T_CLIENTE;
		limAux, auxIDVenta : T_VENTA_ID;
		auxVenta : T_VENTA;
		
	BEGIN
		cursorOff;
		infoBox('Seleccione una opcion.. ', VENTAS_COLOR, FALSE);
		tituloBox('ABM Ventas', VENTAS_COLOR);
		Box(1,79,4,19);
		Writeln('1 - Alta');
		Writeln('2 - Baja');
		Writeln('3 - Modificar');
		Writeln('4 - Historial de compras de un cliente');
		Writeln('5 - Lista de Ventas');
		Writeln('6 - Lista Ventas Ordenadas por Fecha');
		Writeln('7 - Ver ventas a un cliente entre rango fechas');
		Writeln('8 - Totales vendidos en rango de fechas');
		Writeln('9 - Ver ventas en rango de fechas');
		Writeln('0 - Ver lista Ventas Inactivas');
		
		Writeln;
		Writeln('ESC - Volver');
		key := readKey;
		cursorOn;
		CASE key OF
			'1'	:	BEGIN
						cargarVenta;
					END;
			'2'	:	BEGIN
						mostrarBajaVenta;
					END;
			'3'	:	BEGIN
						auxVenta:= mostrarBuscarVenta;
						mostrarModificarVenta(auxVenta);
					END;
			'4' :	BEGIN
						auxCli:= mostrarBuscarCliente;
						IF auxCli.id >= 0 THEN BEGIN
							buscarVentasCliente(auxCli, TRUE, auxArr, limAux);
							mostrarListaVentas(auxArr, limAux, TRUE, 'Lista Ventas Inc Inactivos');
						END;
					END;
			'5' :	mostrarListaVentas;
			'6' :	BEGIN
						auxArr:= listaVentas;
						listaVentasFecha(auxArr);
						mostrarListaVentas(auxArr, ultIDVenta, FALSE);
					END;
			'7'	:	BEGIN
						mostrarVentasEnRangoFechas(TRUE);
						tituloBox('Seleeccione un ID para ver en detalles', VENTAS_COLOR);
						Box(1,79,4,19);
						REPEAT
							Write('Seleccione un  ID para ver detalles: ');
							readln(auxIDVenta);
						UNTIL auxIDVenta IN[0..ultIDVenta];
						mostrarVenta(listaVentas[auxIDVenta]);
						readkey;
					END;
			'8' :	BEGIN
						mostrarTotalesVendidos;
					END;
			'9' :	BEGIN
						mostrarVentasEnRangoFechas(FALSE);
					END;
					
			'0' :	BEGIN
						mostrarListaVentas(listaVentas, ultIDVenta, TRUE, 'Lista Inactivos');
					END;
		END;
	END;
BEGIN
	inicializar;
END.
