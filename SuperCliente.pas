{
* Trabajo Final: Supermercado (c)2014.
* Alan Asmis, Franco Cafferata, Leonardo Chaia.
* https://bitbucket.org/lospibes/supermercado
* 
* This work is licensed under the Creative Commons 
* Attribution-NonCommercial-NoDerivatives 4.0 International License. 
* To view a copy of this license, visit 
* http://creativecommons.org/licenses/by-nc-nd/4.0/.
* }
UNIT SuperCliente;
INTERFACE
	USES SysUtils, CRT, SuperConfig, SuperGraphics;
	CONST
		TOT_CLI = DEFAULT_TOTAL_ARRAY;
		CLI_ARCH = DATA_FOLDER + '/archCliente.dat';
		CLI_POR_PAG = 2;
		
	TYPE
		T_CLI_ID = -1..TOT_CLI;
		T_CLI_DNI = LONGWORD;

		T_CLIENTE = RECORD
			id			: T_CLI_ID;
			nombre 		: T_STR;
			apellido	: T_STR;
			dni			: T_CLI_DNI;
			telefono	: LONGWORD;
			domicilio	: T_LONG_STR;
			email		: T_LONG_STR;
			estaActivo  : BOOLEAN;
		END;

		T_CLI_ARR = ARRAY[T_CLI_ID] OF T_CLIENTE;
		T_CLI_ARCH = FILE OF T_CLIENTE;

	{ Procedimientos publicos}
	PROCEDURE ABMClientes;
	FUNCTION mostrarBuscarCliente : T_CLIENTE;
	FUNCTION getCliente(CONST idCliente : T_CLI_ID) : T_CLIENTE;	
	PROCEDURE mostrarCliente(cliente : T_CLIENTE);
	FUNCTION getFullName(CONST cliente : T_CLIENTE) : T_STR;

IMPLEMENTATION

	VAR
		archCliente : T_CLI_ARCH;
		ultIDCliente : T_CLI_ID;
		listaClientes : T_CLI_ARR;
	
	PROCEDURE inicializar;
	BEGIN
		assign(archCliente, CLI_ARCH);
		IF NOT DirectoryExists(DATA_FOLDER) THEN
			mkdir(DATA_FOLDER);

		IF NOT FileExists(CLI_ARCH) THEN
			reWrite(archCliente)
		ELSE
			reSet(archCliente);

		ultIDCliente:= -1;
		WHILE NOT EOF(archCliente) DO
		BEGIN
			inc(ultIDCliente);
			seek(archCliente, ultIDCliente);
			read(archCliente, listaClientes[ultIDCliente]);
		END;
		close(archCliente);
	END;
	
	FUNCTION getFullName(CONST cliente : T_CLIENTE) : T_STR;
	BEGIN
		getFullName:= cliente.nombre + ' ' + cliente.apellido;
	END;
	
	FUNCTION clienteVacio : T_CLIENTE;
	VAR
		cliente : T_CLIENTE;
	BEGIN
		WITH cliente DO
		BEGIN
			id			:= -1;
			nombre		:= '';
			apellido 	:= '';
			dni			:= 0;
			telefono	:= 0;
			email		:= '';
			domicilio	:= '';
			estaActivo 	:= FALSE;
		END;
		clienteVacio:= cliente;
	END;

	PROCEDURE mostrarCliente(cliente : T_CLIENTE);
	BEGIN
		WITH cliente DO
		BEGIN
			Writeln('-- Cliente #', id, ' ---');
			TextColor(WHITE);
			Write('  ', apellido , ' ', nombre);
			IF NOT estaActivo THEN
			BEGIN
				TextColor(RED);
				Write(' (* INACTIVO *)');
			END;
			TextColor(DEFAULT_COLOR);
			Writeln;
			Writeln('   DNI: ',dni);
			Writeln('   Telefono: ', telefono);
			Writeln('   Domicilio: ', domicilio);
			Writeln('   Email: ', email);
			Writeln;
		END
	END;
	
	PROCEDURE mostrarListaClientes(VAR lista : T_CLI_ARR; mostrarInactivos : BOOLEAN);
	VAR
		i: T_CLI_ID;
		cantMostrada: 0..CLI_POR_PAG;
		posY : BYTE;
		key : CHAR;
	BEGIN
		cursorOff;
		tituloBox('Lista Clientes (' + #17 + ' ' + #16 + ' para navegar, ESC salir)', CLIENTES_COLOR);
		Box(1,79,4,19,DEFAULT_BG, DEFAULT_COLOR,0);;
		cantMostrada:= 0;
		posY:= 4;
		i:= -1;
		IF ultIDCliente < 0 THEN
		BEGIN
			Writeln('No hay clientes ingresados..');
			infoBox('ERROR: No hay clientes ingresados', RED, TRUE);
			Readln;
		END
		ELSE
			REPEAT
				IF i <= ultIDCliente THEN
					inc(i);
				WITH lista[i] DO
					IF estaActivo OR (NOT estaActivo AND mostrarInactivos) THEN
					BEGIN
						inc(cantMostrada);
						Box(1,79, posY,9,DEFAULT_BG, DEFAULT_COLOR,1);
						posY:= posY + 10;
						mostrarCliente(lista[i]);
						IF (cantMostrada = CLI_POR_PAG) OR ( (cantMostrada <> CLI_POR_PAG) AND (i = ultIDCliente))THEN
						BEGIN
							posY:= 4;
							IF (cantMostrada <> CLI_POR_PAG) THEN
								Box(1,79, posY + 10,9,DEFAULT_BG, DEFAULT_COLOR,0);
							infoBox('Mostrando '+ intToStr(cantMostrada)+ ' de '+intToStr(ultIDCliente + 1)+' a partir del ID#'+ intToStr(i - cantMostrada + 1), CLIENTES_COLOR, FALSE);
							key := Readkey;				
							IF key = #0 THEN
							BEGIN
								key := Readkey;
								IF key = KEY_LEFT THEN
									IF ( (i - (cantMostrada + CLI_POR_PAG)) > 0 ) THEN
										i:= i - (cantMostrada + CLI_POR_PAG)
									ELSE
										i:=-1
								ELSE IF (i = ultIDCliente) THEN
									i:= i - cantMostrada;
							END;
							cantMostrada:= 0;
						END;
					END;
			UNTIL (key = KEY_ESC);
		cursorOn;
	END;

	PROCEDURE mostrarListaClientes(VAR lista : T_CLI_ARR);
	BEGIN
		mostrarListaClientes(lista, FALSE);
	END;
	
	PROCEDURE mostrarListaClientes;
	BEGIN
		mostrarlistaClientes(listaClientes, FALSE);
	END;
	
	PROCEDURE listaClientesDNI (VAR arr: T_CLI_ARR);
	VAR
		i,j : T_CLI_ID;
		auxCli : T_CLIENTE;
		cambioUltVuelta : BOOLEAN;
	BEGIN
		i:= -1; 
		cambioUltVuelta:= TRUE;
		WHILE (i < ultIDCliente - 1) AND (i < TOT_CLI) AND cambioUltVuelta DO
		BEGIN
			inc(i);
			cambioUltVuelta:= FALSE;
			FOR j:= 0 TO ultIDCliente - i -1 DO //VERY IMPORTANT -1
				IF arr[j].dni > arr[j + 1].dni THEN
				BEGIN
					cambioUltVuelta:= TRUE;
					auxCli := arr[j];
					arr[j] := arr[j + 1];
					arr[j + 1] := auxCli;
				END;
		END;
	END;
	
	PROCEDURE listaClientesApellido (VAR arr: T_CLI_ARR);
	VAR
		i,j : T_CLI_ID;
		auxCli : T_CLIENTE;
		cambioUltVuelta : BOOLEAN;
	BEGIN
		i:= -1; 
		cambioUltVuelta:= TRUE;
		WHILE (i < ultIDCliente - 1) AND (i < TOT_CLI) AND cambioUltVuelta DO
		BEGIN
			inc(i);
			cambioUltVuelta:= FALSE;
			FOR j:= 0 TO ultIDCliente - i -1 DO
				IF upCase(arr[j].apellido + ' ' + arr[j].nombre) > 
					upCase(arr[j + 1].apellido + ' ' + arr[j + 1].nombre) THEN
				BEGIN
					cambioUltVuelta:= TRUE;
					auxCli := arr[j];
					arr[j] := arr[j + 1];
					arr[j + 1] := auxCli;
				END;
		END;
	END;
	
	FUNCTION buscarCliente(buscado : T_CLI_DNI; incInactivos : BOOLEAN) : T_CLIENTE;
	VAR
		cliOut : T_CLIENTE;
		arr : T_CLI_ARR;
		primero, ultimo, medio : T_CLI_ID;
	BEGIN
		arr:= listaClientes;
		listaClientesDNI(arr);
		cliOut := clienteVacio;
		primero:=0;
		ultimo:=ultIDCliente;
		WHILE (primero <= ultimo) AND (cliOut.id = -1) DO
			BEGIN
				medio:= (primero + ultimo) DIV 2;
				IF (arr[medio].dni = buscado) AND (arr[medio].estaActivo
					OR (NOT arr[medio].estaActivo AND incInactivos)) THEN
					cliOut:= arr[medio]
				ELSE
				BEGIN
					IF (arr[medio].dni > buscado) THEN
						ultimo:= medio - 1
					ELSE
						primero:= medio + 1;
				END;
			END;
		buscarCliente := cliOut;
	END;
	
	FUNCTION buscarCliente(buscado : T_CLI_DNI) : T_CLIENTE;
	BEGIN
		buscarCliente:= buscarCliente(buscado, FALSE);
	END;
	
	FUNCTION getCliente(CONST idCliente : T_CLI_ID) : T_CLIENTE;
	BEGIN
		getCliente:= listaClientes[idCliente];
	END;

	FUNCTION mostrarBuscarCliente : T_CLIENTE;
	VAR
		auxDNI : T_CLI_DNI;
		cliAux : T_CLIENTE;
	BEGIN
		infoBox('Ingrese el DNI de un cliente para buscar', CLIENTES_COLOR, TRUE);
		tituloBox('Buscar Cliente', CLIENTES_COLOR);
		Box(1,79,4,19);
		Write('DNI : ');
		Readln(auxDNI);
		cliAux:= buscarCliente(auxDNI);
		IF cliAux.id < 0 THEN 
		BEGIN
			infoBox('No se ha encontrado ningun cliente con ese DNI ',RED,TRUE);
			tituloBox('ERROR: Cliente NO encontrado', RED);
			Box(1,79,4,19);
			Writeln('Cliente no encontrado');
			Readln;
		END;
		mostrarBuscarCliente:= cliAux;
	END;
	
	FUNCTION modificarCliente(VAR cli : T_CLIENTE) : BOOLEAN;
	VAR
		cliAux : T_CLIENTE;
	BEGIN
		infoBox('Cambie solo los datos que desea modificiar (vacio/0 no modificar)', CLIENTES_COLOR, TRUE);
		tituloBox('Modificar Cliente: ' + cli.nombre + ' ' + cli.apellido, CLIENTES_COLOR);
		Box(1,79,4,19);
		cliAux:= clienteVacio;
		mostrarCliente(cli);
		Writeln;
		WITH cliAux DO
		BEGIN
			Write('Nuevo DNI: ');
			Readln(dni);
			Write('Nuevo Nombre: ');
			Readln(nombre);
			Write('Nuevo Apellido: ');
			Readln(apellido);
			Write('Nuevo Telefono: ');
			Readln(telefono);			
			Write('Nuevo Domicilio: ');
			Readln(domicilio);
			Write('Nuevo Email: ');
			Readln(email);
			estaActivo:= cli.estaActivo;
			id:= cli.id;
			
			IF nombre = '' THEN
				nombre:= cli.nombre;
			IF apellido = '' THEN
				apellido:= cli.apellido;
			IF dni = 0 THEN
				dni:= cli.dni;
			IF telefono = 0 THEN
				telefono:= cli.telefono;
			IF domicilio = '' THEN
				domicilio:= cli.domicilio;
			IF email = '' THEN
				email:= cli.email;
		END;
		
		Box(1,79,4,19);
		Writeln('--- Cliente Original ---');
		mostrarCliente(cli);
		Writeln('--- Cliente con Modificaciones ---');
		mostrarCliente(cliAux);
		Write('Desea aplicar los cambios? s/n');
		IF readkey = 's' THEN
		BEGIN
			listaClientes[cli.id]:= cliAux;
			ReSet(archCliente);
			Seek(archCliente, cli.id);
			Write(archCliente,cliAux);
			Close(archCliente);
			TextColor(Green);
			infoBox('El cliente '+ cliAux.nombre+ ' '+ cliAux.apellido+ ' ha sido modificado con exito', CLIENTES_COLOR, FALSE);
			modificarCliente:= TRUE;
			modificarCliente:= TRUE;
		END
		ELSE BEGIN
			TextColor(Red);
			infoBox('El cliente '+ cliAux.nombre+ ' '+ cliAux.apellido+ ' NO ha sido modificado. Nada ha cambiado', RED, FALSE);
			modificarCliente:= FALSE;
		END;
		TextColor(DEFAULT_COLOR);
		Readln;
	END;
	
	PROCEDURE altaBajaCliente(VAR cliente : T_CLIENTE; nuevoEstado : BOOLEAN);
	BEGIN
		IF cliente.id >= 0 THEN
		BEGIN
			cliente.estaActivo:= nuevoEstado;
			listaClientes[cliente.id]:= cliente;
			reset(archCliente);
			Seek(archCliente, cliente.id);
			Write(archCliente, cliente);
			close(archCliente);
		END;
	END;
	
	PROCEDURE mostrarAltaBajaCliente(VAR cliente : T_CLIENTE; esAlta : BOOLEAN);
	VAR
		c : CHAR;
	BEGIN
		IF cliente.id >= 0 THEN
		BEGIN
			infoBox('', CLIENTES_COLOR, FALSE);
			IF esAlta THEN
				tituloBox('Alta Cliente', CLIENTES_COLOR)
			ELSE
				tituloBox('Baja Cliente', CLIENTES_COLOR);
			Box(1,79, 4,19,DEFAULT_BG, DEFAULT_COLOR,1);
			mostrarCliente(cliente);
			IF esAlta THEN
			BEGIN
				TextColor(LightGreen);
				Writeln('Estas por dar de ALTA el cliente ', cliente.nombre,' ', cliente.apellido);
			END
			ELSE BEGIN
				TextColor(Red);
				Writeln('Estas por dar de BAJA el cliente ', cliente.nombre,' ', cliente.apellido);
			END;
			TextColor(DEFAULT_COLOR);
			Writeln;
			Writeln('Desea continuar? s/n');
			c := Readkey;
			Box(1,79, 4,19,DEFAULT_BG, DEFAULT_COLOR,1);
			IF c = 's' THEN
			BEGIN
				altaBajaCliente(cliente, esAlta);
				IF esAlta THEN BEGIN
					TextColor(LightGreen);
					Writeln('El cliente ', cliente.nombre,' ', cliente.apellido,' ha sido dado de alta con exito');
					infoBox('El cliente '+cliente.nombre+' '+ cliente.apellido+' ha sido dado de alta con exito', CLIENTES_COLOR, FALSE);
				END
				ELSE BEGIN
					TextColor(Red);
					Writeln('El cliente ', cliente.nombre,' ', cliente.apellido,' ha sido dado de BAJA con exito');
					infoBox('El cliente '+cliente.nombre+' '+ cliente.apellido+' ha sido dado de BAJA con exito', CLIENTES_COLOR, FALSE);
				END;
				TextColor(DEFAULT_COLOR);
			END
			ELSE BEGIN 
				Writeln('No se ha realizado ningun cambio');
				infoBox('No se ha realizado ningun cambio', CLIENTES_COLOR, FALSE);
			END;
			Readkey;
		END;
	END;
	
	FUNCTION evaluarNuevoCliente : T_CLI_DNI;
	VAR
		auxDNI : T_CLI_DNI;
		cliAux : T_CLIENTE;
		c : CHAR;
	BEGIN
		tituloBox('Cargar Cliente', CLIENTES_COLOR);
		Box(1,79,4,19);
		Writeln('Cliente ID #', ultIDCliente + 1);
		Write('  DNI: ');
		Readln(auxDNI);
		IF auxDNI > 0 THEN
			cliAux:= buscarCliente(auxDNI, TRUE);
		IF (cliAux.id >= 0) AND (auxDni > 0) THEN
		BEGIN
			infoBox('Ya existe un cliente con ese DNI.', RED, TRUE);
			tituloBox('ERROR: El cliente ya existe',RED);
			Box(1,79,4,19);
			TextColor(DEFAULT_COLOR);
			Writeln('Ya existe un cliente con ese DNI.');
			mostrarCliente(cliAux);
			IF NOT cliAux.estaActivo THEN
			BEGIN
				Write('Este cliente se encuentra ');
				TextColor(Red);
				Write('INACTIVO');
				TextColor(DEFAULT_COLOR);
				Writeln(', desea darlo de alta nuevamente? s/n');
				c:= readkey;
				IF c = 's' THEN
					mostrarAltaBajaCliente(cliAux, TRUE);
			END
			ELSE IF c <> 's' THEN
			BEGIN
				Writeln('El cliente existe y se encuentra activo..');
				Writeln('Desea modificarlo? s/n');
				c:= readkey;
				IF c = 's' THEN
					modificarCliente(cliAux);
			END;
			auxDNI:= 0;
		END;
		evaluarNuevoCliente := auxDNI;
	END;
	
	PROCEDURE cargaCliente;
	VAR
		auxDNI : T_CLI_DNI;
	BEGIN
		infoBox('Salir con DNI = 0', CLIENTES_COLOR, TRUE);
		auxDNI := evaluarNuevoCliente;
		Reset(archCliente);
		WHILE (auxDNI > 0) AND (ultIDCliente < TOT_CLI)  DO
		BEGIN
			Inc(ultIDCliente);
			WITH listaClientes[ultIDCliente] DO
			BEGIN
				id:= ultIDCliente;
				dni:= auxDNI;
				Write('  Nombre: ');
				Readln(nombre);
				Write('  Apellido: ');
				Readln(apellido);
				Write('  Telefono: ');
				Readln(telefono);
				Write('  Domicilio: ');
				Readln(domicilio);
				Write('  Email: ');
				Readln(email);
				estaActivo:= TRUE;
			END;
			Seek(archCliente, ultIDCliente);
			Write(archCliente, listaClientes[ultIDCliente]);

			auxDNI:= evaluarNuevoCliente;
		END;
		Close(archCliente);
	END;
	
	PROCEDURE ABMClientes;
	VAR
		cliAux : T_CLIENTE;
		arrAux : T_CLI_ARR;
		key    : CHAR;
	BEGIN
		cursorOff;
		infoBox('Seleccione una opcion.. ', CLIENTES_COLOR, TRUE);
		tituloBox('ABM Clientes', CLIENTES_COLOR);
		Box(1,79,4,19);
		Writeln('1 - Alta');
		Writeln('2 - Baja');
		Writeln('3 - Modificar');
		Writeln('4 - Buscar');
		Writeln('5 - Lista Clientes');
		Writeln('6 - Lista Ordenada Clientes');
		Writeln;
		Writeln('7 - Lista Clientes incluyendo Inactivos');
		Writeln('0 - Volver');
		key := readKey;
		cursorOn;
		CASE key OF
			'1'	:	cargaCliente;
			'2'	:	BEGIN
						cliAux:= mostrarBuscarCliente;
						IF cliAux.id >= 0 THEN
							mostrarAltaBajaCliente(cliAux, FALSE)
					END;
			'3'	:	BEGIN
						cliAux:= mostrarBuscarCliente;
						IF cliAux.id >= 0 THEN
							modificarCliente(cliAux)
					END;
			'4' :   BEGIN
						cliAux:= mostrarBuscarCliente;
						infoBox('', CLIENTES_COLOR, TRUE);
						IF cliAux.id >= 0 THEN
						BEGIN
							infoBox('', CLIENTES_COLOR, TRUE);
							tituloBox('Cliente encontrado: ' + cliAux.nombre + ' '+ cliAux.apellido, CLIENTES_COLOR);
							Box(1,79,4,19);
							mostrarCliente(cliAux);
							cursorOff;
							Readln;
							cursorOn;
						END
					END;
			'5'	:	mostrarListaClientes;					
			'6'	:	BEGIN
						arrAux:= listaClientes;
						listaClientesApellido(arrAux);
						mostrarListaClientes(arrAux);
					END;
			'7'	:   mostrarListaClientes(listaClientes, TRUE);
		END;//Case
	END;
BEGIN
	inicializar;
END.
