{
* Trabajo Final: Supermercado (c)2014.
* Alan Asmis, Franco Cafferata, Leonardo Chaia.
* https://bitbucket.org/lospibes/supermercado
* 
* This work is licensed under the Creative Commons 
* Attribution-NonCommercial-NoDerivatives 4.0 International License. 
* To view a copy of this license, visit 
* http://creativecommons.org/licenses/by-nc-nd/4.0/.
* }
UNIT SuperProductos;
INTERFACE
	USES SysUtils, CRT, SuperConfig, SuperGraphics;
	CONST
		TOT_PROD = DEFAULT_TOTAL_ARRAY;
		PROD_ARCH = DATA_FOLDER + '/archProductos.dat';
		
	TYPE
		T_PROD_ID = -1..TOT_PROD;

		T_PRODUCTO = RECORD
			id			: T_PROD_ID;
			desc 		: T_LONG_STR;
			stock		: WORD;
			precio		: REAL;
			estaActivo  : BOOLEAN;
		END;

		T_PROD_ARR = ARRAY[T_PROD_ID] OF T_PRODUCTO;

	{ Procedimientos publicos}
	PROCEDURE ABMProductos;
	FUNCTION mostrarBuscarProducto : T_PRODUCTO;
	PROCEDURE mostrarProducto(producto : T_PRODUCTO; unaLinea : BOOLEAN);
	PROCEDURE modificarStock(VAR prod : T_PRODUCTO; CONST cant : INTEGER);
	PROCEDURE modificarStock(CONST idProducto : T_PROD_ID; CONST cant : INTEGER);
	FUNCTION GetProducto(CONST idProducto : T_PROD_ID) : T_PRODUCTO;

IMPLEMENTATION
	CONST 
		PROD_POR_PAG = 16;
	TYPE
		T_PROD_ARCH = FILE OF T_PRODUCTO;
	VAR
		archProducto : T_PROD_ARCH;
		ultIDProducto : T_PROD_ID;
		listaProductos : T_PROD_ARR;
	
	PROCEDURE inicializar;
	BEGIN
		assign(archProducto, PROD_ARCH);
		IF NOT DirectoryExists(DATA_FOLDER) THEN
			mkdir(DATA_FOLDER);

		IF NOT FileExists(PROD_ARCH) THEN
			reWrite(archProducto)
		ELSE
			reSet(archProducto);

		ultIDProducto:= -1;
		WHILE NOT EOF(archProducto) DO
		BEGIN
			inc(ultIDProducto);
			seek(archProducto, ultIDProducto);
			read(archProducto, listaProductos[ultIDProducto]);
		END;
		close(archProducto);
	END;
	
	FUNCTION productoVacio : T_PRODUCTO;
	VAR
		producto : T_PRODUCTO;
	BEGIN
		WITH producto DO
		BEGIN
			id			:= -1;
			desc		:= '';
			stock		:= 0;
			precio		:= 0;
			estaActivo 	:= FALSE;
		END;
		productoVacio:= producto;
	END;

	PROCEDURE mostrarProducto(producto : T_PRODUCTO; unaLinea : BOOLEAN);
	BEGIN
		WITH producto DO
		BEGIN
			IF unaLinea THEN
			BEGIN
				IF NOT estaActivo THEN
					TextColor(Red)
				ELSE IF stock = 0 THEN
					TextColor(Yellow)
				ELSE IF stock <= 3 THEN
					TextColor(Brown);
				Write('| #', id);
				GoToXY(8,WhereY);
				Write('| ', desc);
				IF NOT estaActivo THEN
					Write('  (*INACT*)');
				GoToXY(50,WhereY);
				Write('| ');
				IF stock <= 3 THEN
					TextColor(Red);
				Write(stock);
				GoToXY(58,WhereY);
				TextColor(DEFAULT_COLOR);
				Write('| ');
				TextColor(Green);
				Write('$',precio:6:2);
				TextColor(DEFAULT_COLOR);
				Write('.-');
				GoToXY(73,WhereY);
				Writeln('|');
			END
			ELSE BEGIN
				Writeln('-- Producto #', id, ' ---');
				TextColor(WHITE);
				Write('  ', desc);
				IF NOT estaActivo THEN
				BEGIN
					TextColor(RED);
					Write(' (* INACTIVO *)');
				END;
				TextColor(DEFAULT_COLOR);
				Writeln;
				Writeln('   Stock: ', stock);
				TextColor(Green);
				Writeln('   Precio: $', precio:4:2,'.-');
				TextColor(DEFAULT_COLOR);
				Writeln;
			END;
		END
	END;
	
	PROCEDURE mostrarProducto(producto : T_PRODUCTO);
	BEGIN
		mostrarProducto(producto, FALSE);
	END;
	
	PROCEDURE mostrarListaProductos(VAR lista : T_PROD_ARR; mostrarInactivos : BOOLEAN);
		PROCEDURE escribirTablaSup;
		BEGIN
			TextColor(White);
			Write('| ID   |');
			GoToXY(22,WhereY);
			Write('DESCRIPCION');
			GoToXY(50,WhereY);
			Write('| STOCK');
			GoToXY(58,WhereY);
			Write('|    PRECIO');
			GoToXY(73,WhereY);
			Writeln('|');
			TextColor(DEFAULT_COLOR);
		END;
	VAR
		i: T_PROD_ID;
		cantMostrada: 0..PROD_POR_PAG;
		key : CHAR;
	BEGIN
		cursorOff;
		tituloBox('Lista Productos (' + #17 + ' ' + #16 + ' para navegar, ESC salir)', PRODUCTOS_COLOR);
		Box(1,79,4,19);
		escribirTablaSup;
		cantMostrada:= 0; 
		i:= -1;
		IF ultIDProducto < 0 THEN
		BEGIN
			Writeln('No hay productos ingresados..');
			infoBox('ERROR: No hay productos ingresados', RED, TRUE);
			Readln;
		END
		ELSE
			REPEAT
				IF i < ultIDProducto THEN
					inc(i);
				WITH lista[i] DO
					IF estaActivo OR (NOT estaActivo AND mostrarInactivos) THEN
					BEGIN
						inc(cantMostrada);
						mostrarProducto(lista[i], TRUE);
						IF (cantMostrada = PROD_POR_PAG) OR ((cantMostrada <> PROD_POR_PAG) AND (i = ultIDProducto)) THEN
						BEGIN
							infoBox('Mostrando '+ intToStr(cantMostrada)+ ' de '+intToStr(ultIDProducto + 1)+' a partir del ID#'+ intToStr(i - cantMostrada + 1), PRODUCTOS_COLOR, FALSE);
							key := Readkey;				
							IF key IN[#0, KEY_ENTER] THEN
							BEGIN
								key := Readkey;
								IF key = KEY_LEFT THEN
									IF ( (i - (cantMostrada + PROD_POR_PAG)) > 0 ) THEN
										i:= i - (cantMostrada + PROD_POR_PAG)
									ELSE
										i:=-1
								ELSE IF (i = ultIDProducto) THEN BEGIN
									IF (i - cantMostrada > 0) THEN
										i:= i - cantMostrada;
									sound(1);
								END;
							END;
							Box(1,79,4,19);
							escribirTablaSup;
							cantMostrada:= 0;
						END;
					END;
			UNTIL (key = KEY_ESC);
		cursorOn;
	END;

	PROCEDURE mostrarListaProductos(VAR lista : T_PROD_ARR);
	BEGIN
		mostrarListaProductos(lista, FALSE);
	END;
	
	PROCEDURE mostrarListaProductos;
	BEGIN
		mostrarListaProductos(listaProductos, FALSE);
	END;
	
	PROCEDURE listaProductosDesc (VAR arr: T_PROD_ARR);
	VAR
		i,j : T_PROD_ID;
		auxProd : T_PRODUCTO;
		cambUltVueltA: BOOLEAN;
	BEGIN
		i:= -1; cambUltVuelta:= TRUE;
		WHILE (i < ultIDProducto -1) AND (i < TOT_PROD) AND cambUltVuelta DO
		BEGIN
			inc(i);
			cambUltVuelta:= FALSE;
			FOR j:= 0 TO ultIDProducto - i -1 DO
				IF upCase(arr[j].desc) > upCase(arr[j + 1].desc) THEN
				BEGIN
					cambUltVuelta:= TRUE;
					auxProd := arr[j];
					arr[j] := arr[j + 1];
					arr[j + 1] := auxProd;
				END;
		END;
	END;
	
	FUNCTION buscarProducto(buscado : T_LONG_STR; incInactivos : BOOLEAN) : T_PRODUCTO;
	VAR
		prodOut : T_PRODUCTO;
		arr : T_PROD_ARR;
		primero, ultimo, medio : T_PROD_ID;
	BEGIN
		arr:= listaProductos;
		listaProductosDesc(arr);
		prodOut := productoVacio;
		primero:=0;
		ultimo:=ultIDProducto;
		buscado:= upCase(buscado);
		WHILE (primero <= ultimo) AND (prodOut.id = -1) DO
		BEGIN
			medio:= (primero + ultimo) DIV 2;
			IF (upCase(arr[medio].desc) = buscado) AND (arr[medio].estaActivo
				OR (NOT arr[medio].estaActivo AND incInactivos)) THEN
				prodOut:= arr[medio]
			ELSE BEGIN
				IF (upCase(arr[medio].desc) > buscado) THEN
					ultimo:= medio - 1
				ELSE
					primero:= medio + 1;
			END;
		END;
		buscarProducto := prodOut;
	END;
	
	FUNCTION buscarProducto(buscado : T_LONG_STR) : T_PRODUCTO;
	BEGIN
		buscarProducto:= buscarProducto(buscado, FALSE);
	END;
	
	FUNCTION buscarProducto(buscado : T_PROD_ID; incInactivos : BOOLEAN) : T_PRODUCTO;
	{VAR
		prodOut : T_PRODUCTO;
		primero, ultimo, medio : T_PROD_ID;}
	BEGIN
	{
		prodOut := productoVacio;
		primero:=0;
		ultimo:=ultIDProducto;
		WHILE (primero <= ultimo) AND (prodOut.id = -1) DO
		BEGIN
			medio:= (primero + ultimo) DIV 2;
			IF (listaProductos[medio].id = buscado) AND (listaProductos[medio].estaActivo
				OR (NOT listaProductos[medio].estaActivo AND incInactivos)) THEN
				prodOut:= listaProductos[medio]
			ELSE BEGIN
				IF (listaProductos[medio].id > buscado) THEN
					ultimo:= medio - 1
				ELSE
					primero:= medio + 1;
			END;
		END;}
		IF buscado <= ultIDProducto  THEN
			buscarProducto := listaProductos[buscado]
		ELSE
			buscarProducto:= productoVacio;
	END;
	
	FUNCTION buscarProducto(buscado : T_PROD_ID) : T_PRODUCTO;
	BEGIN
		buscarProducto:= buscarProducto(buscado, FALSE);
	END;

	FUNCTION mostrarBuscarProducto : T_PRODUCTO;
	VAR
		auxDesc : T_LONG_STR;
		auxID	: T_PROD_ID;
		prodAux : T_PRODUCTO;
		key		: CHAR;
	BEGIN
		cursorOff;
		prodAux := productoVacio;
		tituloBox('Buscar producto', PRODUCTOS_COLOR);
		infoBox('Seleccione metodo de busqueda..', PRODUCTOS_COLOR, TRUE);
		Box(1,79,4,19);
		Writeln('Como desea buscar?');
		Writeln;
		Writeln('1 - ID');
		Writeln('2 - Descripcion');
		Writeln;
		Writeln('0 - Volver');
		key:= readkey;
		cursorOn;
		CASE key OF
			'1'	:	BEGIN
						infoBox('Ingrese el ID de un producto para buscar', PRODUCTOS_COLOR, TRUE);
						tituloBox('Buscar Producto via ID', PRODUCTOS_COLOR);
						Box(1,79,4,19);
						Write('ID : ');
						Readln(auxID);
						prodAux:= buscarProducto(auxID);
					END;
			'2'	:	BEGIN
						infoBox('Ingrese la descripcion de un producto para buscar', PRODUCTOS_COLOR, TRUE);
						tituloBox('Buscar Producto via Descripcion', PRODUCTOS_COLOR);
						Box(1,79,4,19);
						Write('Descripcion : ');
						Readln(auxDesc);
						prodAux:= buscarProducto(auxDesc);
					END;
		END;
		IF (prodAux.id < 0) AND (key IN ['1', '2']) THEN 
		BEGIN
			infoBox('No se ha encontrado ningun producto con esa descripcion',PRODUCTOS_COLOR,TRUE);
			tituloBox('ERROR: Producto NO encontrado', RED);
			Box(1,79,4,19);
			Writeln('Producto no encontrado');
			sound(1);
			Readln;
		END;
		mostrarBuscarProducto:= prodAux;
	END;
	
	FUNCTION modificarProducto(VAR prod : T_PRODUCTO) : BOOLEAN;
	VAR
		prodAux : T_PRODUCTO;
		key		: CHAR;
	BEGIN
		infoBox('Seleccione que campo desea modificar utilizando los numeros', PRODUCTOS_COLOR, TRUE);
		tituloBox('Modificar Producto: '+ prod.desc, PRODUCTOS_COLOR);
		Box(1,79,4,19);
		prodAux:= prod;
		WITH prod DO 
		BEGIN
			Writeln('Que campo desea modificar?');
			Writeln;
			Writeln('1) Descripcion: ',desc);
			Writeln('2) Stock: ',stock);
			Write('3) Precio:');
			TextColor(Green);
			Writeln('$',precio:4:2,'.-');
			TextColor(DEFAULT_COLOR)
		END;
		
		REPEAT
			infoBox('Que desea modificar? (1..3)',PRODUCTOS_COLOR, FALSE);
			key:= Readkey;
		UNTIL strToInt(key) IN [1..3];
		CASE key OF
			'1'	:	BEGIN
						infoBox('Modificar descripcion',PRODUCTOS_COLOR, TRUE);
						Box(1,79,4,19);
						Writeln('Producto original');
						Writeln;
						mostrarProducto(prod);
						Write('Nueva Descripcion: ');
						Readln(prodAux.desc);
					END;
			'2'	:	BEGIN
						infoBox('Modificar stock',PRODUCTOS_COLOR, TRUE);
						Box(1,79,4,19);
						Writeln('Producto original');
						Writeln;
						mostrarProducto(prod);
						Write('Nuevo Stock: ');
						Readln(prodAux.stock);
					END;
			'3'	:	BEGIN
						infoBox('Modificar precio',PRODUCTOS_COLOR, TRUE);
						Box(1,79,4,19);
						Writeln('Producto original');
						Writeln;
						mostrarProducto(prod);
						Write('Nuevo Precio: $');
						Readln(prodAux.precio);
					END;
		END;
		
		Box(1,79,4,19);
		Writeln('--- Producto Original ---');
		mostrarProducto(prod);
		Writeln('--- Producto con Modificaciones ---');
		mostrarProducto(prodAux);
		Writeln('Desea aplicar los cambios? s/n');
		IF readkey = 's' THEN
		BEGIN
			listaProductos[prod.id]:= prodAux;
			ReSet(archProducto);
			Seek(archProducto, prod.id);
			Write(archProducto,prodAux);
			Close(archProducto);
			TextColor(Green);
			infoBox('El producto '+ prodAux.desc + ' ha sido modificado con exito', GREEN, FALSE);
			modificarProducto:= TRUE;
		END
		ELSE BEGIN
			TextColor(Red);
			infoBox('El Producto '+ prodAux.desc +  ' NO ha sido modificado. Nada ha cambiado', RED, FALSE);
			modificarProducto:= FALSE;
		END;
		TextColor(DEFAULT_COLOR);
		Readln;
	END;
	
	PROCEDURE altaBajaProducto(VAR producto : T_PRODUCTO; nuevoEstado : BOOLEAN);
	BEGIN
		IF producto.id >= 0 THEN
		BEGIN
			producto.estaActivo:= nuevoEstado;
			listaProductos[producto.id]:= producto;
			reset(archProducto);
			Seek(archProducto, producto.id);
			Write(archProducto, producto);
			close(archProducto);
		END;
	END;
	
	PROCEDURE mostrarAltaBajaProducto(VAR producto : T_PRODUCTO; esAlta : BOOLEAN);
	VAR
		c : CHAR;
	BEGIN
		IF producto.id >= 0 THEN
		BEGIN
			infoBox('',PRODUCTOS_COLOR, FALSE);
			IF esAlta THEN
				tituloBox('Alta Producto', PRODUCTOS_COLOR)
			ELSE
				tituloBox('Baja Producto', PRODUCTOS_COLOR);
			Box(1,79, 4,19,DEFAULT_BG, DEFAULT_COLOR,1);
			mostrarProducto(producto);
			IF esAlta THEN
			BEGIN
				TextColor(LightGreen);
				Writeln('Estas por dar de ALTA el producto ', producto.desc);
			END
			ELSE BEGIN
				TextColor(Red);
				Writeln('Estas por dar de BAJA el producto ', producto.desc);
			END;
			TextColor(DEFAULT_COLOR);
			Writeln;
			Writeln('Desea continuar? s/n');
			c := Readkey;
			Box(1,79, 4,19,DEFAULT_BG, DEFAULT_COLOR,1);
			IF c = 's' THEN
			BEGIN
				altaBajaProducto(producto, esAlta);
				IF esAlta THEN BEGIN
					TextColor(LightGreen);
					Writeln('El producto ',producto.desc,' ha sido dado de alta con exito');
					infoBox('El producto '+producto.desc+' ha sido dado de alta con exito',GREEN, FALSE)
				END
				ELSE BEGIN
					TextColor(Red);
					Writeln('El producto ', producto.desc,' ha sido dado de BAJA con exito');
					infoBox('El producto '+producto.desc+' ha sido dado de BAJA con exito',GREEN, FALSE);
				END;
				TextColor(DEFAULT_COLOR);
			END
			ELSE BEGIN 
				Writeln('No se ha realizado ningun cambio');
				infoBox('No se ha realizado ningun cambio',PRODUCTOS_COLOR, FALSE);
			END;
			Readkey;
		END;
	END;
	
	FUNCTION evaluarNuevoProducto : T_LONG_STR;
	VAR
		auxDesc : T_LONG_STR;
		prodAux : T_PRODUCTO;
		c : CHAR;
	BEGIN
		tituloBox('Cargar Producto', PRODUCTOS_COLOR);
		Box(1,79,4,19);
		Writeln('--- Producto ID #', ultIDProducto + 1,' ---');
		Write('  Descripcion: ');
		Readln(auxDesc);
		IF auxDesc <> '' THEN
			prodAux:= buscarProducto(auxDesc, TRUE);
		IF (prodAux.id >= 0) AND (auxDesc <> '') THEN
		BEGIN
			infoBox('Ya existe un producto con esa descripcion.',RED,TRUE);
			tituloBox('ERROR: El producto ya existe',RED);
			Box(1,79,4,19);
			TextColor(DEFAULT_COLOR);
			Writeln('Ya existe un producto con esa descripcion.');
			mostrarProducto(prodAux);
			sound(1);
			IF NOT prodAux.estaActivo THEN
			BEGIN
				Write('Este producto se encuentra ');
				TextColor(Red);
				Write('INACTIVO');
				TextColor(DEFAULT_COLOR);
				Writeln(', desea darlo de alta nuevamente? s/n');
				c:= readkey;
				IF c = 's' THEN
					mostrarAltaBajaProducto(prodAux, TRUE);
			END
			ELSE IF c <> 's' THEN
			BEGIN
				Writeln('El producto existe y se encuentra activo..');
				Writeln('Desea modificarlo? s/n');
				c:= readkey;
				IF c= 's' THEN
					modificarProducto(prodAux);
			END;
			auxDesc:= '';
		END;
		evaluarNuevoProducto := auxDesc;
	END;
	
	PROCEDURE cargarProducto;
	VAR
		auxDesc : T_LONG_STR;
	BEGIN
		infoBox('Salir con desc vacia', PRODUCTOS_COLOR, FALSE);
		auxDesc := evaluarNuevoProducto;
		Reset(archProducto);
		WHILE (auxDesc <> '') AND (ultIDProducto < TOT_PROD)  DO
		BEGIN
			Inc(ultIDProducto);
			WITH listaProductos[ultIDProducto] DO
			BEGIN
				id:= ultIDProducto;
				desc:= auxDesc;
				Write('  Stock :');
				Readln(stock);
				Write('  Precio ');
				TextColor(Green);
				Write('$');
				Readln(precio);
				TextColor(DEFAULT_COLOR);
				estaActivo:= TRUE;
			END;
			Seek(archProducto, ultIDProducto);
			Write(archProducto, listaProductos[ultIDProducto]);

			auxDesc:= evaluarNuevoProducto;
		END;
		Close(archProducto);
	END;
	
	PROCEDURE ABMProductos;
	VAR
		prodAux : T_PRODUCTO;
		arrAux : T_PROD_ARR;
		key    : CHAR;
	BEGIN
		cursorOff;
		infoBox('Seleccione una opcion.. ', PRODUCTOS_COLOR, FALSE);
		tituloBox('ABM Productos', PRODUCTOS_COLOR);
		Box(1,79,4,19);
		Writeln('1 - Alta');
		Writeln('2 - Baja');
		Writeln('3 - Modificar');
		Writeln('4 - Buscar');
		Writeln('5 - Lista Productos');
		Writeln('6 - Lista Ordenada Productos');
		Writeln;
		Writeln('7 - Lista Productos incluyendo Inactivos');
		Writeln('0 - Volver');
		key := readKey;
		cursorOn;
		CASE key OF
			'1'	:	cargarProducto;
			'2'	:	BEGIN
						prodAux:= mostrarBuscarProducto;
						IF prodAux.id >= 0 THEN
							mostrarAltaBajaProducto(prodAux, FALSE)
					END;
			'3'	:	BEGIN
						prodAux:= mostrarBuscarProducto;
						IF prodAux.id >= 0 THEN
							modificarProducto(prodAux)
					END;
			'4' :   BEGIN
						prodAux:= mostrarBuscarProducto;
						infoBox('', PRODUCTOS_COLOR, TRUE);
						IF prodAux.id >= 0 THEN
						BEGIN
							infoBox('',PRODUCTOS_COLOR,TRUE);
							tituloBox('Producto encontrado: ' + prodAux.desc, PRODUCTOS_COLOR);
							Box(1,79,4,19);
							mostrarProducto(prodAux);
							cursorOff;
							Readln;
							cursorOn;
						END
					END;
			'5'	:	mostrarListaProductos;					
			'6'	:	BEGIN
						arrAux:= listaProductos;
						listaProductosDesc(arrAux);
						mostrarListaProductos(arrAux);
					END;
			'7'	:   mostrarListaProductos(listaProductos, TRUE);
		END;//Case
	END;
	
	PROCEDURE modificarStock(VAR prod : T_PRODUCTO; CONST cant : INTEGER);
	VAR
		auxInt : INTEGER;  //Es necesario porque el stock es un WORD
	BEGIN
		auxInt:= prod.stock + cant;
			IF auxInt >= 0 THEN
			prod.stock:= prod.stock + cant
		ELSE
			prod.stock:= 0;
			
		listaProductos[prod.id]:= prod;
		Reset(archProducto);
		Seek(archProducto, prod.id);
		Write(archProducto, prod);
		Close(archProducto);
	END;
	
	PROCEDURE modificarStock(CONST idProducto : T_PROD_ID; CONST cant : INTEGER);
	BEGIN
		modificarStock(listaProductos[idProducto], cant);
	END;
	
	FUNCTION GetProducto(CONST idProducto : T_PROD_ID) : T_PRODUCTO;
	BEGIN
		GetProducto:= listaProductos[idProducto];
	END;

BEGIN
	inicializar;
END.

