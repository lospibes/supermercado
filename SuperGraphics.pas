{
* Trabajo Final: Supermercado (c)2014.
* Alan Asmis, Franco Cafferata, Leonardo Chaia.
* https://bitbucket.org/lospibes/supermercado
* 
* This work is licensed under the Creative Commons 
* Attribution-NonCommercial-NoDerivatives 4.0 International License. 
* To view a copy of this license, visit 
* http://creativecommons.org/licenses/by-nc-nd/4.0/.
* }
UNIT SuperGraphics;
INTERFACE
	USES crt, dateUtils, sysUtils;
	CONST
		KEY_ENTER = #13;
		KEY_ESC = #27;
		KEY_LEFT = #75;
		KEY_RIGHT = #77;
		KEY_UP = #72;
		KEY_DOWN = #80;
		
		DEFAULT_BG = Black;
		DEFAULT_COLOR = LightGray;
		DEFAULT_HL_BG = LightGray;
		DEFAULT_HL_COLOR = Black;
		
	PROCEDURE Box(PIX,Ancho,PIY,Largo,Fondo,Texto,Recuadro, xPadd, yPadd:BYTE);
	PROCEDURE Box(PIX,Ancho,PIY,Largo,Fondo,Texto,Recuadro:byte);
	PROCEDURE Box(PIX,Ancho,PIY,Largo:BYTE);
	PROCEDURE Box(PIX,Ancho,PIY,Largo, xPadd, yPadd:BYTE);
	PROCEDURE IntroBox;
	PROCEDURE infoBox (c: STRING; color:BYTE; centrado : BOOLEAN);
	PROCEDURE tituloBox (c: STRING);
	PROCEDURE tituloBox (c: STRING; color : BYTE);
	FUNCTION leerFecha : TDateTime;
	PROCEDURE escribirPrecio(precio : REAL);
	
IMPLEMENTATION

	PROCEDURE Box(PIX,Ancho,PIY,Largo,Fondo,Texto,Recuadro, xPadd, yPadd:BYTE);
	VAR 
		Lineas_hor,Lineas_ver :BYTE;
		r1,r2,r3,r4,r5,r6     :CHAR;
	BEGIN
		window(PIX,PIY,PIX + Ancho-1, PIY +Largo-1);
		textBackground(Fondo);
		ClrScr;
		textColor(Texto);
		window(1,1,80,25);
		CASE Recuadro OF 
			0:	BEGIN
					r1:=(' ');
					r2:=(' ');
					r3:=(' ');
					r4:=(' ');
					r5:=(' ');
					r6:=(' ');
				END;
			1:	BEGIN

					r1:=(#218);//esquina derecha superior
					r2:=(#192);//esquina derecha inferor 
					r3:=(#196);//lineas verticales
					r4:=(#191);//linea izquierda superior
					r5:=(#217);//linea isquierda inferior
					r6:=(#179);//lineas horizontales 
				END;
			2:	BEGIN
					r1:=('*');//esquina derecha superior
					r2:=('*');//esquina derecha inferor 
					r3:=('.');//lineas verticales
					r4:=('*');//esquina izquierda superior
					r5:=('*');//esquina izquierda inferior
					r6:=(':');//lineas horizontales 
				END;
			3:	BEGIN
					r1:=(#201);
					r2:=(#200);
					r3:=(#205);
					r4:=(#187);
					r5:=(#188);
					r6:=(#186);
				END;
		 END;
		 gotoxy(PIX ,PIY );write(r1);
		 gotoxy(PIX , PIY+Largo-1);write(r2);
		 gotoxy(Ancho + PIX , PIY);write(r4); 
		 gotoxy(Ancho+PIX , Largo+PIY-1);write(r5);
		 FOR Lineas_ver:=(PIX+1) TO (PIX + Ancho-1)DO 
		 BEGIN
			gotoxy(Lineas_ver,PIY);write(r3);
			gotoxy(Lineas_ver,PIY + Largo-1);write(r3);
		 END;
		 FOR Lineas_hor:=(PIY+1) TO (PIY  + Largo-2)DO
		 BEGIN
			gotoxy(PIX,Lineas_hor);write(r6);
			gotoxy(PIX + Ancho,Lineas_hor);write(r6);
		 END;
		 
		Window(PIX + xPadd, PIY + yPadd,80,23);
	END;
	
	PROCEDURE Box(PIX,Ancho,PIY,Largo,Fondo,Texto,Recuadro:byte);
	BEGIN
		Box(PIX,Ancho,PIY,Largo,Fondo,Texto,1, 4 ,1);
	END;
	
	PROCEDURE Box(PIX,Ancho,PIY,Largo:BYTE);
	BEGIN
		Box(PIX,Ancho,PIY,Largo,DEFAULT_BG,DEFAULT_COLOR,1, 4, 1);
	END;
	
	PROCEDURE Box(PIX,Ancho,PIY,Largo, xPadd, yPadd:BYTE);
	BEGIN
		Box(PIX,Ancho,PIY,Largo,DEFAULT_BG,DEFAULT_COLOR,1, xPadd, yPadd);
	END;
	
	PROCEDURE infoBox (c: string; color:byte; centrado : BOOLEAN);
	var centro:integer;
	BEGIN
		box (1,78,23,3,color,white,0);
		IF centrado THEN BEGIN
			centro:=(78 - length(c)) div 2;
			gotoxy(centro,WhereY);
		END
		ELSE
			gotoxy(3,WhereY);
		Write(c);
	END;

	PROCEDURE tituloBox (c: string; color : BYTE);
	var centro:integer;
	BEGIN
		c:= '--- '+ Upcase(c) +' ---';
		box (1,79,1,3,color,white,1);
		centro:=(79 - length(c)) div 2;
		gotoxy(centro,WhereY);
		write(c);
	END;
	
	PROCEDURE tituloBox (c: string);
	BEGIN
		tituloBox(c, GREEN);
	END;
	
	PROCEDURE IntroBox;
	VAR
		x      :Byte; 		
	BEGIN
		cursoroff;
		x:=5;
		box(1,78,1,25,Green,white,3);
		gotoxy(x,6);
		writeln('Alumnos:             :      Profesores: ');
		delay(500);
		gotoxy(x,10);
		writeln('Asmis Alan Noe.      :      Roxana Zosa Cito.');
		delay(500);
		gotoxy(x,14);
		writeln('Cafferata Franco.    :      Lourdes Pralong.');
		delay(500);
		gotoxy(x,18);
		writeln('Chaia Leonardo.      :      Walter Bell.');
		delay(2000);
		textbackground(black);
		clrscr;
		cursoron;
	END;
	
	FUNCTION leerFecha : TDateTime;
	VAR
		auxStr : STRING[32];
	BEGIN
		Writeln('Ingrese fecha (ej: 01 01 2000; hoy; a 2009)');
		Writeln('Comandos: "hoy", "a YYYY"');
		REPEAT
			Readln(auxStr);
		UNTIL auxStr <> '';
		IF auxStr = 'hoy' THEN
			leerFecha:= Today
		ELSE IF UpCase(auxStr[1]) = 'A' THEN BEGIN
			Delete(auxStr,1,2);
			auxStr:= '01 01 ' + auxStr;
			leerFecha:= ScanDateTime('DD MM YYYY',auxStr,DefaultFormatSettings)
		END
		ELSE BEGIN
			//TRY
			leerFecha:= ScanDateTime('DD MM YYYY',auxStr,DefaultFormatSettings);
{
			EXCEPT
				ON EConvertError DO BEGIN
					Writeln('Fecha Invalida');
				END;
			END;
}
		END;
	END;
	
	PROCEDURE escribirPrecio(precio : REAL);
	BEGIN
		TextColor(Green);
		Write('$',formatFloat('#######.##',precio),'.-');
		TextColor(DEFAULT_COLOR);
	END;
END.
