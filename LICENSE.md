
## Trabajo Final: Supermercado (c)2014.
* Alan Asmis,
* Franco Cafferata,
* Leonardo Chaia.

https://bitbucket.org/lospibes/supermercado

This work is licensed under the Creative Commons 
Attribution-NonCommercial-NoDerivatives 4.0 International License. 
To view a copy of this license, visit 
http://creativecommons.org/licenses/by-nc-nd/4.0/.
